const gulp = require('gulp');
const gulpSass = require('gulp-sass');
const sass = require('node-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');

gulpSass.compile = sass;

const sass_path = './sass/**/*.scss';
const css_path = './css';

gulp.task('sass', () => {
    return gulp.src(sass_path)
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(concat('index.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(css_path));
});

gulp.task('sass:watch', () => {
    gulp.watch(sass_path, gulp.series(['sass']));
});

gulp.task('dev', gulp.series(['sass', 'sass:watch']))